class AddAttachmentImageToArticles < ActiveRecord::Migration[4.2]
  def self.up
    change_table :articles do |t|
    #add_attachment :articles, :image
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :articles, :image
  end
end

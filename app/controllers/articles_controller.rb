class ArticlesController < ApplicationController

  before_action :set_article, only: [:edit, :update, :show, :destroy]
  before_action :authenticate_user!, except: [:index, :show]
  
  def index
  @articles = current_user.articles.order('id desc').paginate(page: params[:page ], per_page: 5)
  end 
	def new
		@article = current_user.articles.new	
	end

	def create
 	 	@article = current_user.articles.new(article_params)
  
  	if @article.save
   	flash[:notice] = "Article was successfully created"
   	redirect_to article_path(@article)
  else
   render 'new'
  end
 end

 def show
  
 end

 def edit

 	
 end

 def update
  
  if @article.update(article_params)
   flash[:notice] = "Article was updated"
   redirect_to article_path(@article)
  else
   flash[:notice] = "Article was not updated"
   render 'edit'
  end
end

def destroy
  @article.destroy
  flash[:notice] = "Article was deleted"
  redirect_to articles_path
 end

private
  def article_params
   params.require(:article).permit(:title, :description, :image)
  end

  def set_article
   @article = Article.find(params[:id])
end
end
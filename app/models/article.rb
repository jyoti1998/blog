class Article < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	belongs_to :user, :optional => true
	validates :title, presence: true, length: { minimum: 3, maximum: 50 }
 validates :description, presence: true, length: { minimum: 10, maximum: 300 } 
 has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
Rails.application.routes.draw do
  devise_for :users
    get '/users/sign_in' => 'user/sessions#destroy'
	root 'pages#index'
	resources :articles do
		resources :comments
	
	end
end
